#Aleksandra Kuriata
include("./blocksys.jl")
using blocksys

function ReadFile(btrue::Bool)
	row = Float64[]
	column = Float64[]
	elementMatrix = Float64[]
	b = Array{Float64}(0)
	n = Float64(1)
#	l = Float64(1)

	file = open("A.txt")
	isFirstLine = true
	while !eof(file)
		line1=readline(file)
		if isFirstLine == true
			isFirstLine = false
			element = split(line1, " ")
			n = parse(Int64,element[1])
			#l = parse(Int64,element[2])
		else
			element = split(line1, " ")
			push!(row,parse(Float64,element[1]))
			push!(column,parse(Float64,element[2]))
			push!(elementMatrix,parse(Float64,element[3]))
		end
	end
	A = sparse(map(Int,row),map(Int,column),elementMatrix)
	close(file)

	if btrue==true
		file = open("b.txt")
		isFirstLine = true
		while !eof(file)
			line1=readline(file)
			if isFirstLine == true
				isFirstLine = false
				line = split(line1, " ")
				n = parse(Int64,line[1])
			else
				element = split(line1, " ")
				push!(b,parse(Float64,element[1]))
			end
		end
		close(file)
		relativeError=true
	else
			b=Array{Float64}(n)
			i =1
			j=1
			while i <= n
				b[i]=0
				#elementMin = i+3*l+1 > n ? n : i+3*l+1
				while j<=n
					b[i]=b[i]+A[i,j] * 1.0
					j=j+1
				 end
				 j=1
				 i=i+1
			 end
			 writedlm("bnew.txt",b)
	end

	#pierwszy bool czy mainElement
	#drugi bool czy wektor prawych stron
  	@time GaussElmination(A,n,true,b)
end
	ReadFile(true)
