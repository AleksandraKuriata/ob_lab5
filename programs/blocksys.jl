#Aleksandra Kuriata
module blocksys
    export Swaprows
    export GaussElmination
    export MainElementChoose
    export VectorOfRightPages

    function Swaprows(A, b, i, n, index)
        column=i
        while column<=n
            A[i,column],A[index,column] = A[index,column],A[i,column]
            column = column+1
        end
        b[i],b[index] = b[index],b[i]
    end

    function MainElementChoose(A,b,n, i)
            index = typemin(Int64)
            searchedMaxValue = typemin(Int64)
            temporaryMax=0

#            elementMin = i+3*l+1 > n ? n : i+3*l+1
            for j = i :n
                t  = A[j,i]
                temporaryMax = max(abs(t),abs(searchedMaxValue))
                if searchedMaxValue < temporaryMax
                    searchedMaxValue = temporaryMax
                    index = j
                end
            end
            Swaprows(A, b, i, n, index)
    end

    function GaussElmination(A::SparseMatrixCSC{Float64,Int64},n::Int64, mainElement::Bool,b::Array{Float64})
        for k = 1: n-1
             if mainElement==true
                 MainElementChoose(A,b,n,k)
            end
            if abs(A[k, k])< eps(Float64)
                  println("Error operation is not allowed")
                  exit(0)
             end
            #elementMin = k+3*l+1 > n ? n : k+3*l+1
      		for row = k + 1:n
      			tmp = A[row, k] / A[k, k]

            #    lowerBound =  k+3*l > n ? n : k+3*l
      			for col = k+1:n
      				A[row, col] -= tmp * A[k, col]
                 end

      			A[row, k] = 0
      			b[row] -= tmp * b[k]
              end
          end
          println("$(A)")
          println("$(b)")

        for k = 1: n
                if abs(A[k, k])< eps(Float64)
                      println("Error operation is not allowed")
                      exit(0)
                 end
        end

        for k = n : -1 : 1
            #elementMin = k-3*l < 1 ? 1 : k-3*l
            b[k] = b[k]/A[k,k]
            for y = k-1 : -1 : n
                b[y] -= A[y,k]*b[k]
            end
         end
          println("$(b)")

            error = norm(b-(fill(Float64(1.0), length(b))))/norm(fill(Float64(1.0),length(b)))
            writedlm("x.txt",error)
		    open("x.txt", "a") do x

			    writedlm(x,b)
           end
       end
end
